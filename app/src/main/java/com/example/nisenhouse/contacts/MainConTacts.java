package com.example.nisenhouse.contacts;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.*;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.*;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.OpenFileActivityBuilder;
import com.google.android.gms.plus.Plus;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;

import model.Contact;

public class MainConTacts extends AppCompatActivity  implements ConnectionCallbacks,
        OnConnectionFailedListener {

    private static final String TAG = "Google Drive Activity";
    private static final int REQUEST_CODE_RESOLUTION = 1;
    private static final  int REQUEST_CODE_OPENER = 2;
    public static final int NAME = 0;
    public static final int POSITION = 1;
    public static final int TEL = 2;
    public static final int EMAIL = 3;
    public static final String MY_CONTACTS_JSON = "/myContacts.json";
    public static final String RESOURCE_PATH_CONF = "/resourcePath.conf";
    public static final String SPREADSHEETS_PREFIX = "https://spreadsheets.google.com/tq?key=";
    public static final String URI_D_PART = "/d/";
    private GoogleApiClient mGoogleApiClient;
    private boolean fileOperation = false;
    private DriveId mFileId;
    public DriveFile file;

    SwipeRefreshLayout mSwipeRefreshLayout;

    private Contacts contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_main_con_tacts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        final TableLayout ll = initTable();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final MainConTacts thisActivity = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                FileChooser fc = new FileChooser(thisActivity);
//                fc.setFileListener(new FileChooser.FileSelectedListener() {
//                    @Override
//                    public void fileSelected(final File file) {
//                        Contacts contacts = readAllContacts(file);
//                        init(ll, contacts);
//                        writeAllContacts(contacts);
//                    }
//                });
//
//                fc.showDialog();

                setConnection();
            }
        });

        EditText filter = (EditText) findViewById(R.id.txtFilter);
        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                resetTable(ll, s.toString());
                //Log.d("Contacts","filtering by " + s);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                     @Override
                                                     public void onRefresh() {
                                                         refreshFromResource();
                                                     }
                                                 });


        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            String url = getResourceUri(data);
            loadFromResource(url);
        } else {
            refreshFromResource();
        }
    }

    private String getResourceUri(Uri data) {
        String uri = data.toString();
        int idx = uri.indexOf(URI_D_PART);
        if (idx < 0) {
            return uri;
        }
        String str = uri.substring(idx + URI_D_PART.length());
        String resource = str.split("/")[0];
        return SPREADSHEETS_PREFIX + resource;
    }

    private void getJson(final TableLayout ll, String jsonUrl) {
        if (jsonUrl == null || jsonUrl.isEmpty()) {
            init(ll, readAllContacts());
            return;
        }

        mSwipeRefreshLayout.setRefreshing(true);
        new DownloadWebpageTask(new AsyncResult() {
            @Override
            public void onResult(JSONObject object) {
                try {
                    List<Contact> contacts = processJson(object);
                    contact = new Contacts();
                    contact.setContacts(contacts);
                    init(ll, contact);
                    writeAllContacts(contact);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Unable to fetch", Toast.LENGTH_LONG).show();
                    init(ll, readAllContacts());
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }).execute(jsonUrl);
    }

    private List<Contact> processJson(JSONObject object) {
        List<Contact> $ = new LinkedList<>();

        try {
            JSONArray rows = object.getJSONArray("rows");

            for (int r = 0; r < rows.length(); ++r) {
                JSONObject row = rows.getJSONObject(r);
                JSONArray columns = row.getJSONArray("c");

                if (countNonNulls(columns) < 3) {
                    continue;
                }

                String name = getStringValue(columns, NAME);
                name = name != null ? name.trim() : null;
                String position = getStringValue(columns, POSITION);
                position = position != null ? position.trim() : null;
                String telephone = getStringValue(columns, TEL);
                telephone = telephone != null ? telephone.trim() : null;
                String email = getStringValue(columns, EMAIL);
                email = email != null ? email.trim() : null;
                Contact contact = new Contact(name, position, telephone, email);
                $.add(contact);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return $;
    }

    private String getStringValue(JSONArray columns, int index){
        try {
            return columns.getJSONObject(index).getString("v");
        } catch (JSONException e) {
            return null;
        }
    }

    private int countNonNulls(JSONArray columns) {
        int nulls = 0;
        for (int c = 0; c < columns.length(); c++) {
            try {
                if (columns.getJSONObject(c) == null) {
                    nulls++;
                }
            } catch (JSONException e) {
                //return c - nulls;
                nulls ++;
            }
        }
        return columns.length() - nulls;
    }

    private Contacts readAllContacts() {
        File file = new File(getAppDir() + MY_CONTACTS_JSON);
        return readAllContacts(file);
    }

    private Contacts readAllContacts(File file) {
        String str;

        try {
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            str = new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (file.getName().endsWith(".json")) {
            return new Gson().fromJson(str, Contacts.class);
        } else {
            List<Contact> contacts = new LinkedList<>();
            for (String line : str.split("\n")) {
                String[] info = line.split(",");
                Log.d("Contacts", "\"" + line + "\" info");
                if (info.length != 4) {
                    Log.d("Contacts", "was in the wrong format");
                    continue;
                }
                Contact contact =
                        new Contact(
                                info[0].trim(),
                                info[1].trim(),
                                info[2].trim(),
                                info[3].trim());
                contacts.add(contact);
            }
            return new Contacts(contacts);
        }
    }



    private void writeAllContacts(Contacts contacts) {
        writeAllContacts(new File(getAppDir() + MY_CONTACTS_JSON), contacts);
    }

    private void writeAllContacts(File file, Contacts contacts) {
        if (contacts == null)
            return;


        try {
            Log.d("Contacts", file.getAbsolutePath());
            if (!file.exists()) {
                Log.d("Contacts", "creating");
                file.createNewFile();
            }
            byte[] data = new Gson().toJson(contacts).getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveResourcePath(String url) {
        saveResourcePath(new File(getAppDir() + RESOURCE_PATH_CONF), url);
    }

    private void saveResourcePath(File file, String url) {
        try {
            Log.d("Contacts", file.getAbsolutePath());
            if (!file.exists()) {
                Log.d("Contacts", "creating");
                file.createNewFile();
            }
            byte[] data = url.getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readResourcePath() {
        File file = new File(getAppDir() + RESOURCE_PATH_CONF);
        return readResourcePath(file);
    }

    private String readResourcePath(File file) {
        String str;

        try {
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();
            return new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void init(TableLayout ll, Contacts con) {
        if (con == null)
            return;
        this.contact = con;

        resetTable(ll);
    }

    private void resetTable(TableLayout ll) {
        resetTable(ll, "");
    }

    private void resetTable(TableLayout ll, String filter) {
        List<Contact> contacts = this.contact.getContacts();
        ll.removeAllViews();
        addHeader(ll);

        int pos = 1;
        for (Contact c : contacts) {
            if (
                    (c.getName() != null && c.getName().contains(filter)) ||
                    (c.getPosition() != null && c.getPosition().contains(filter)) ||
                    (c.getEmail() != null && c.getEmail().contains(filter)) ||
                    (c.getTelephone() != null && c.getTelephone().contains(filter)))
                addRow(ll, pos++, c.getName(), c.getPosition(), c.getTelephone(), c.getEmail());
        }
    }

    @NonNull
    private TableLayout initTable() {
        TableLayout ll = (TableLayout) findViewById(R.id.displayLinear);
        ll.setColumnStretchable(0, true);
        ll.setColumnStretchable(1, true);
        ll.setColumnStretchable(2, true);
        return ll;
    }

    private void addHeader(TableLayout ll) {
        TableRow row = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);
        TextView txtName = new TextView(this);
        txtName.setText("שם");
        row.addView(txtName);
        TextView txtPos = new TextView(this);
        txtPos.setText("תפקיד");
        row.addView(txtPos);
        TextView txtTel = new TextView(this);
        txtTel.setText("טלפון");
        row.addView(txtTel);
        ll.addView(row, 0);
    }

    private void addRow(TableLayout ll, int idx, final String name, final String position, final String telephone, final String email) {

        final MainConTacts thisActivity = this;

        TableRow row = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);
        TextView txtName = new TextView(this);
        txtName.setText(name);
        row.addView(txtName);
        TextView txtPos = new TextView(this);
        txtPos.setText(position);
        txtPos.setTextColor(Color.parseColor("#aa0000"));

        txtPos.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
//                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//                i.putExtra(Intent.EXTRA_TEXT   , "body of email");
                try {
                    startActivity(Intent.createChooser(i, "שלח דוא\"ל ל" + position + " באמצעות..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(thisActivity, "לא נמצאו אפליקציות למשלוח דוא\"ל", Toast.LENGTH_SHORT).show();
                }
            }
        });

        row.addView(txtPos);
        TextView txtTel = new TextView(this);
        txtTel.setText(telephone);
        txtTel.setTextColor(Color.parseColor("#0000aa"));

        txtTel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + telephone));
                try {
                    startActivity(
                            Intent.createChooser(
                                    phoneIntent,
                                    "התקשר ל" + name + ", " + position + " דרך..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(thisActivity, "לא נמצאו אפליקציות לביצוע שיחות טלפון", Toast.LENGTH_SHORT).show();
                }
            }
        });

        TableLayout.LayoutParams tableRowParams =
                new TableLayout.LayoutParams
                        (TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);

        tableRowParams.setMargins(10, 10, 10, 10);
        row.setLayoutParams(tableRowParams);

        row.addView(txtTel);

        ll.addView(row, idx);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getAppDir() {
        PackageManager m = getPackageManager();
        String $ = null;
        try {
            PackageInfo p = m.getPackageInfo(getPackageName(), 0);
            $ = p.applicationInfo.dataDir;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("Contacts", "Error Package name not found ", e);
        }
        return $;
    }

    public static class Contacts {
        public void setContacts(List<Contact> contacts) {
            this.contacts = contacts;
        }

        private List<Contact> contacts;

        private Contacts() {
            contacts = new LinkedList<>();
        }

        public Contacts(List<Contact> contacts) {
            this.contacts = contacts;
        }

        public List<Contact> getContacts() {
            return contacts;
        }
    }

    private void setConnection() {
//        if (mGoogleApiClient != null) {
//            if(mGoogleApiClient.isConnected()) {
//                mGoogleApiClient.disconnect();
//                mGoogleApiClient = null;
//            }
//            mGoogleApiClient.clearDefaultAccountAndReconnect();
//        }
        if (mGoogleApiClient == null) {

            /**
             * Create the API client and bind it to an instance variable.
             * We use this instance as the callback for connection and connection failures.
             * Since no account name is passed, the user is prompted to choose.
             */
            mGoogleApiClient = new Builder(this)
                    .addApi(Drive.API)
                    .addApi(Plus.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }

//        if (mGoogleApiClient.hasConnectedApi(Auth.GOOGLE_SIGN_IN_API)) {
//            mGoogleApiClient.clearDefaultAccountAndReconnect();
//        }


        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.clearDefaultAccountAndReconnect();
        } else {
        }
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        disconect();
        super.onPause();
    }

    private void disconect() {
        if (mGoogleApiClient != null) {

            // disconnect Google Android Drive API connection.
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {

        // Called whenever the API client fails to connect.
        Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());

        if (!result.hasResolution()) {

            // show the localized error dialog.
            GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
            return;
        }

        /**
         *  The failure has a resolution. Resolve it.
         *  Called typically when the app is not yet authorized, and an  authorization
         *  dialog is displayed to the user.
         */

        try {

            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);

        } catch (IntentSender.SendIntentException e) {

            Log.e(TAG, "Exception while starting resolution activity", e);
        }
    }

    /**
     * It invoked when Google API client connected
     * @param connectionHint
     */
    @Override
    public void onConnected(Bundle connectionHint) {
//        mGoogleApiClient.clearDefaultAccountAndReconnect();

        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();

        fileOperation = false;

        // create new contents resource
        Drive.DriveApi.newDriveContents(mGoogleApiClient)
                .setResultCallback(driveContentsCallback);
    }

    /**
     * It invoked when connection suspended
     * @param cause
     */
    @Override
    public void onConnectionSuspended(int cause) {

        Log.i(TAG, "GoogleApiClient connection suspended");
    }

    /**
     * This is Result result handler of Drive contents.
     * this callback method call CreateFileOnGoogleDrive() method
     * and also call OpenFileFromGoogleDrive() method, send intent onActivityResult() method to handle result.
     */
    final ResultCallback<DriveContentsResult> driveContentsCallback =
            new ResultCallback<DriveContentsResult>() {
        @Override
        public void onResult(DriveContentsResult result) {

            if (result.getStatus().isSuccess()) {

                if (fileOperation == true) {

                    //CreateFileOnGoogleDrive(result);

                } else {

                    OpenFileFromGoogleDrive();

                }
            }
        }
    };

    /**
     *  Open list of folder and file of the Google Drive
     */
    public void OpenFileFromGoogleDrive(){

        IntentSender intentSender = Drive.DriveApi
                .newOpenFileActivityBuilder()
                .setMimeType(new String[] { "application/vnd.google-apps.spreadsheet" })
//                .setSelectionFilter(Filters.not(Filters.eq(com.google.android.gms.drive.query.SearchableField.)))
             .build(mGoogleApiClient);
        try {

            startIntentSenderForResult(
                    intentSender, REQUEST_CODE_OPENER, null, 0, 0, 0);

        } catch (IntentSender.SendIntentException e) {

            Log.w(TAG, "Unable to send intent", e);
        }
    }

    /**
     * Handle Response of selected file
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(final int requestCode,
                                    final int resultCode, final Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CODE_RESOLUTION:
                setConnection();
                break;
            case REQUEST_CODE_OPENER:

                if (resultCode == RESULT_OK) {

                    mFileId = (DriveId) data.getParcelableExtra(
                            OpenFileActivityBuilder.EXTRA_RESPONSE_DRIVE_ID);

                    Log.e("file id", mFileId.getResourceId() + "");

                    String url = SPREADSHEETS_PREFIX + mFileId.getResourceId();

                    loadFromResource(url);
                    saveResourcePath(url);
                }

                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void refreshFromResource() {
        String resource = readResourcePath();

//        if (resource == null) {
//            throw new IllegalArgumentException();
//        }
        loadFromResource(resource);
    }

    private void loadFromResource(String url) {
        getJson((TableLayout) findViewById(R.id.displayLinear), url);
    }
}
